<?php
/**
 * @file
 * Actual function bodies for default definitions for OpenERP feed fetcher
 * 
 */
/**
 * OpenERP_Fetcher class
 */
class OpenERP_Fetcher extends FeedsFetcher {
  public function fetch(FeedsSource $source) {
  // todo
  }
  
/**
 * OpenERP_Fetcher configuration form
 * 
 */
  
  public function configForm(&$form_state) {
    $models = array();
  $models = _openerp_get_models();
  //var_dump($models);
  $types = node_get_types('names');
  
  $form = array(
  'fetcher' => array(
  '#type' => 'fieldset',
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
  
  'openerp_models' => array(
      '#type' => 'select',
      '#title' => t('OpenERP Models'),
      '#description' => t('Select the model for the items to be created from.'),
      '#options' => $models,
      '#default_value' => variable_get('openerp_model', NULL),
    ),
  'openerp_offset' => array(
      '#type' => 'textfield',
      '#title' => t('Starting offset'),
    '#size' => 10,
      '#description' => t('Select starting offset for fetch from selected models.'),
      '#default_value' => variable_get('openerp_offset', 0),
    ),
  'openerp_limit' => array(
      '#type' => 'textfield',
      '#title' => t('Fetching limit'),
    '#size' => 10,
      '#description' => t('Select limit for fetch from selected models.'),
      '#default_value' => variable_get('openerp_limit', 50),
    ),
  'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit fetcher settings'),
    '#description' => t('Select limit for fetch from selected models.'),
      '#submit' => array('openerp_fetcher_submit'),
    ),
    )
  );
  
    return $form;
  }
}
/**
 * Feeds_OpenERP_Batch class
 */
class Feeds_OpenERP_Batch extends FeedsImportBatch {
  protected $url;
  protected $file_path;
  /**
   * Constructor.
   */
  public function __construct($url = NULL, $feed_nid) {
    $this->url = $url;
    parent::__construct('', $feed_nid);
  }
  /**
   * Implementation of FeedsImportBatch::getRaw();
   */
  public function getRaw() {
  $offset = variable_get('openerp_offset', 0);
  $limit  = variable_get('openerp_limit', 50);
  $openerp_model = variable_get('openerp_model', NULL);
  $proxy = OpenERPProxy::getProxy();
  if ( ($r = $proxy->page($openerp_model, array(), (int)$offset, (int)$limit)) === FALSE ) {
  // error, print error message
   drupal_set_message(OpenERPProxy::message());
  }
  else { 
  // just test
    return $r;
       } // else
  }
}
/**
 * OpenERP_Fetcher configuration form callback
 * todo - move it to user_variables module
 */
function openerp_fetcher_submit($form, &$form_data) {
  variable_set('openerp_limit', $form_data['values']['openerp_limit']);
  variable_set('openerp_offset', $form_data['values']['openerp_offset']);
  variable_set('openerp_model', $form_data['values']['openerp_models']);
  drupal_set_message(t('Fetcher configurations are successfully saved.'));
}
