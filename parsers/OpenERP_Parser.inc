<?php

/**
 * @file
 * OpenERP Parser plugin.
 */

/**
 * Feeds parser plugin that parses OpenERP model arrays.
 */
class OpenERP_Parser extends FeedsParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
 /*   
	feeds_include_library('opml_parser.inc', 'opml_parser');
    $result = opml_parser_parse($batch->getRaw());
    $batch->title = $result['title'];
    $batch->items = $result['items'];
*/
	}

  /**
   * Set the total for a stage.
   */
  public function setTotal($stage, $total) {
    $this->total[$stage] = $total;
  }

  /**
   * Get the total for a stage.
   */
  public function getTotal($stage) {
    return $this->total[$stage];
  }
	
  /**
   * Return mapping sources.
   */
  public function getMappingSources() {

  }
}
