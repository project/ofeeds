<?php
/**
 * @file
 * Actual function bodies for default definitions for OpenERP feed processor
 * 
 */

/**
 * OpenERP_Processor class
 */


 
class OpenERP_Processor extends FeedsProcessor {

  public function process(FeedsImportBatch $batch, FeedsSource $source) {
  // todo
  }
  
  public function clear(FeedsBatch $batch, FeedsSource $source) {
  //todo
  }
  
/**
 * OpenERP_Processor configuration form
 * 
 */
  
  /* moved to OpenERP_Fetcher.inc
  
  public function configForm(&$form_state) {
    $models = array();
  $models = _openerp_get_models();
  //var_dump($models);
  $types = node_get_types('names');
  
  $form = array(
  'processor' => array(
  '#type' => 'fieldset',
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
  
  'openerp_models' => array(
      '#type' => 'select',
      '#title' => t('OpenERP Models'),
      '#description' => t('Select the model for the items to be created from.'),
      '#options' => $models,
      '#default_value' => variable_get('openerp_model', NULL),
    ),
  'openerp_offset' => array(
      '#type' => 'textfield',
      '#title' => t('Starting offset'),
    '#size' => 10,
      '#description' => t('Select starting offset for fetch from selected models.'),
      '#default_value' => variable_get('openerp_offset', 0),
    ),
  'openerp_limit' => array(
      '#type' => 'textfield',
      '#title' => t('Fetching limit'),
    '#size' => 10,
      '#description' => t('Select limit for fetch from selected models.'),
      '#default_value' => variable_get('openerp_limit', 50),
    ),
  'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit processor settings'),
    '#description' => t('Select limit for fetch from selected models.'),
      '#submit' => array('openerp_processor_submit'),
    ),

    )
  );
  
    return $form;
  }

*/

}

/**
 * OpenERP_Processor configuration form callback
 * todo - move it to user_variables module
 */

 /* moved to OpenERP_Fetcher.inc
 
function openerp_processor_submit($form, &$form_data) {
  variable_set('openerp_limit', $form_data['values']['openerp_limit']);
  variable_set('openerp_offset', $form_data['values']['openerp_offset']);
  variable_set('openerp_model', $form_data['values']['openerp_models']);
  drupal_set_message(t('Processor configurations are successfully saved.'));
}

*/